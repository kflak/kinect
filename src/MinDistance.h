#ifndef _MINDISTANCE
#define _MINDISTANCE

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxOsc.h"
#include "ofxKinect.h"


class MinDistance : public ofxKinect {
    public: 
        void setup(ofxKinect * _kinect, string _host, int _port);
        void update();
        void draw();

        float alpha = 0.9;
        ofVec2f prevMinDist; 
        ofVec2f minDist;

        ofxCvGrayscaleImage grayImage; // grayscale depth image 
        ofxCvContourFinder contourFinder;
        
        MinDistance();
        
        ofxKinect * kinect;

        string host;
        int port;

        bool sendOSC = true;
        ofxOscSender oscSender;
        ofxOscReceiver oscReceiver;

    private:
};
#endif
