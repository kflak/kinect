#include "ofApp.h"

void ofApp::setup() {

    // enable depth->video image calibration
    kinect.setRegistration(true);

    kinect.init();

    kinect.open();		// opens first available kinect

    if(kinect.isConnected()) {
        ofLogNotice() << "sensor-emitter dist: " << kinect.getSensorEmitterDistance() << "cm";
        ofLogNotice() << "sensor-camera dist:  " << kinect.getSensorCameraDistance() << "cm";
        ofLogNotice() << "zero plane pixel size: " << kinect.getZeroPlanePixelSize() << "mm";
        ofLogNotice() << "zero plane dist: " << kinect.getZeroPlaneDistance() << "mm";
    }

    ofSetFrameRate(60);

    // zero the tilt on startup
    angle = 0;
    kinect.setCameraTiltAngle(angle);

    // start from the front

    minDistance.setup(&kinect, HOST, PORT);
    gestureFollower.setup(&kinect);

}

//--------------------------------------------------------------
void ofApp::update() {

    ofBackground(30); 
    kinect.update(); 
    minDistance.update();
    gestureFollower.update();
}

//--------------------------------------------------------------
void ofApp::draw() {

    ofSetColor(255, 255, 255);

    // visualize closest point to kinect
    // minDistance.draw();
    gestureFollower.draw();

}


//--------------------------------------------------------------
void ofApp::exit() {
    kinect.setCameraTiltAngle(0); // zero the tilt on exit
    kinect.close();

}

//--------------------------------------------------------------
void ofApp::keyPressed (int key) {
    switch (key) {
        case OF_KEY_UP:
            angle++;
            if(angle>30) angle=30;
            kinect.setCameraTiltAngle(angle);
            break;

        case OF_KEY_DOWN:
            angle--;
            if(angle<-30) angle=-30;
            kinect.setCameraTiltAngle(angle);
            break;
    }
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{

}
