#pragma once

#include "ofMain.h"
#include "ofxKinect.h"
#include "ofxGVF.h"
#include "MinDistance.h"
#include "GestureFollower.h"

#define HOST "localhost"
#define PORT 57120
#define RECEIVE_PORT 5000
#define NUM_BLOBS 1

class ofApp : public ofBaseApp {
    public:

        void setup();
        void update();
        void draw();
        void exit();

        void keyPressed(int key);
        void mouseDragged(int x, int y, int button);
        void mousePressed(int x, int y, int button);
        void mouseReleased(int x, int y, int button);
        void mouseEntered(int x, int y);
        void mouseExited(int x, int y);
        void windowResized(int w, int h);

        ofxKinect kinect; 

        int angle; 

        MinDistance minDistance;

        GestureFollower gestureFollower;

};
