#include "MinDistance.h"

MinDistance::MinDistance(){
    kinect = nullptr;
}

void MinDistance::setup(ofxKinect *_kinect, string _host, int _port){

    this->kinect = _kinect; 

    prevMinDist.set(-1, -1);

    grayImage.allocate(kinect->width, kinect->height);

    host = _host;
    port = _port;
    oscSender.setup(host, port);
}

void MinDistance::update(){
    // the basic method: which pixel is closest
    int step = 2;
    int minDistance = 3000;
    for(int x_ = 0; x_ < kinect->width; x_+=step){
        for (int y_ = 0; y_ < kinect->height; y_+=step){
            int kd = kinect->getDistanceAt(x_, y_);
            if (kd>0 && kd<minDistance) {
                minDistance = kd;
                minDist.set(x_, y_);
            } 
        }
    }

    // smoothing
    minDist = minDist + alpha * (prevMinDist - minDist);
    prevMinDist = minDist; 

    // send the positions to SuperCollider
    if(sendOSC) {
        ofxOscMessage m;
        m.setAddress("/kinect/minDistance");
        m.addIntArg(ofMap(minDist.x, 0, 640, 1.0, 0.0));
        m.addIntArg(ofMap(minDist.y, 0, 480, 1.0, 0.0));
        oscSender.sendMessage(m, false); 
    }

}

void MinDistance::draw(){
    ofNoFill();
    ofDrawCircle(
            ofMap(minDist.x, 0, 640, ofGetWidth(), 0),
            ofMap(minDist.y, 0, 480, 0, ofGetHeight()),
            24);

} 
