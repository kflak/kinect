#pragma once

#include "ofxGVF.h"
#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxOsc.h"
#include "ofxKinect.h"

class GestureFollower {
    public:
        void setup(ofxKinect * _kinect);
        void update();
        void draw();
        void learn();

        GestureFollower();

        vector<float> theSample;
        ofxGVF *gvf;
        GVFGesture gesture;

        int threshold = 1000;

        ofxCvGrayscaleImage grayImage;
        ofxCvContourFinder contours;

        ofxKinect * kinect;
        int maxDepth = 1000;

    private:
};
