#include "GestureFollower.h"

GestureFollower::GestureFollower(){
    kinect = nullptr;
}

void GestureFollower::setup(ofxKinect * _kinect){
    this->kinect = _kinect;
    grayImage.allocate(kinect->width, kinect->height);
    gvf = new ofxGVF(); 
}

void GestureFollower::update(){

    ofPixels depth = kinect->getDepthPixels();
    for(auto i : depth){
        if (i > 0 && i < threshold) { 
            depth[i] = 255;
        } else {
            depth[i] = 0;
        }
    };
    grayImage.setFromPixels(depth);
    // grayImage.threshold(threshold); 
    contours.findContours(grayImage, 20, (340*240)/3, 10, false);

    if (gvf->getState()==ofxGVF::STATE_FOLLOWING) {
        if (gesture.getNumberOfTemplates() > 0) {
            gvf->update(gesture.getLastObservation());
        }
    }
}

void GestureFollower::draw(){
    grayImage.draw(0, 0);
    // for (int i = 0; i < contours.nBlobs; i++){
    //     contours.blobs[i].draw(0, 0); 
    // }
}

void GestureFollower::learn(){
    if(gvf->getState() != ofxGVF::STATE_LEARNING) {
        gvf->setState(ofxGVF::STATE_LEARNING);
        gvf->addGestureTemplate(gesture);
        gvf->setState(ofxGVF::STATE_FOLLOWING); 
    }
}

